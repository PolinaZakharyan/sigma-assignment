from sys import argv



def super_fibonacci(n, m):
    if n < m:
        return 1
    queue = [1 for _ in range(m)]
    for i in range(n-m):
        queue.append(sum(queue))
        queue.pop(0)
    return queue[-1]


n, m = map(int, argv[1:3])
print(super_fibonacci(n, m))
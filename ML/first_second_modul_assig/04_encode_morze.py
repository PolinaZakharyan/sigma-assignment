from sys import argv

O = '^'
T = '^^^'
TOKEN_SEP = '_'
LETTER_SEP = TOKEN_SEP * 3
WORD_SEP = TOKEN_SEP * 7

ALPHABET = {
    'A': (O, T),
    'B': (T, O, O, O),
    'C': (T, O, T, O),
    'D': (T, O, O),
    'E': (O),
    'F': (O, O, T, O),
    'G': (T, T, O),
    'H': (O, O, O, O),
    'I': (O, O),
    'J': (O, T, T, T),
    'K': (T, O, T),
    'L': (O, T, O, O),
    'M': (T, T),
    'N': (T, O),
    'O': (T, T, T),
    'P': (O, T, T, O),
    'Q': (T, T, O, T),
    'R': (O, T, O),
    'S': (O, O, O),
    'T': (T),
    'U': (O, O, T),
    'V': (O, O, O, T),
    'W': (O, T, T),
    'X': (T, O, O, T),
    'Y': (T, O, T, T),
    'Z': (T, T, O, O),
    '1': (O, T, T, T, T),
    '2': (O, O, T, T, T),
    '3': (O, O, O, T, T),
    '4': (O, O, O, O, T),
    '5': (O, O, O, O, O),
    '6': (T, O, O, O, O),
    '7': (T, T, O, O, O),
    '8': (T, T, T, O, O),
    '9': (T, T, T, T, O),
    '0': (T, T, T, T, T),
}





def encode_letter(letter):
    return TOKEN_SEP.join(ALPHABET[letter.upper()])

def encode_word(word):
    return LETTER_SEP.join((encode_letter(letter) for letter in word))

def encode_string(string):
    return WORD_SEP.join((encode_word(word) for word in string.split()))

encode_morze = encode_string
print(encode_string(argv[1]))

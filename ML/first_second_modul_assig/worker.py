class Worker:

    def __init__(self, surname, experience, workTime) -> None:
        self.surname = surname
        self.experience = experience
        self.workTime = workTime
        self.salary = 0

    def setSalary(self):
        exp = self.experience
        wT = self.workTime
        s = self.salary
        if exp < 5:
            s = wT * 10
        elif exp >= 5 and exp <= 10:
            s = wT * 20
        else:
            s = wT * 30
        return f"Worker {self.surname} earns salary {s}"
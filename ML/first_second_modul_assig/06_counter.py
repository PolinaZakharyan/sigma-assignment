from sys import argv

def counter(a, b):
    s_a = set(str(a))
    s_b = set(str(b))
    c = len(s_a.intersection(s_b))
    return c

print(counter(*argv[1:3]))
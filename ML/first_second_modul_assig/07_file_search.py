from sys import argv
from os import path

class FsTree:
    def __init__(self, data: list, parentpath="") -> None:
        self.data = data
        self.path = path.join(parentpath, self.data[0])

    def files(self):
        return (item for item in self.data if isinstance(item, str))

    def dirs(self):
        return (item for item in self.data if isinstance(item, list))

    def nodes(self):
        return (FsTree(item, self.path) for item in self.dirs())

def file_search(folder, filename):
    queue = [FsTree(folder)]

    while queue:
        item: FsTree = queue.pop(0)
        if filename in item.files():
            return path.join(item.path, filename)
        queue.extend(item.nodes())
    return False



print(file_search(['polina','sigma', 'counter.py'], 'counter.py'))
print(file_search(['lol', 'kek', ['polina','sigma', 'counter.py']], 'counter.py'))

class Bear:
    def eats(self):
        return 'berries'

class Rabbit:
    def eats(self):
        return 'clover'

class Moomin:
    def eats(self):
        return 'fish'


b = Bear()
print(b.eats())
r = Rabbit()
print(r.eats())
m = Moomin()
print(m.eats())
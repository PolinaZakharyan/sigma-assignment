from typing import Tuple


class Component:
    def does(self):
        raise NotImplementedError("Abstract class")

class Laser(Component):
    def does(self):
        return 'desintegrate'

class Claw(Component):
    def does(self):
        return 'crush'

class SmartPhone(Component):
    def does(self):
        return 'ring'

class Robot:
    def __init__(self):
        self.components: Tuple[Component] = Laser(), Claw(), SmartPhone()

    def does(self):
        return ', '.join(comp.does() for comp in self.components)
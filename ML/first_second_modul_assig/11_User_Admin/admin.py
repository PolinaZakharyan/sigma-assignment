from user import User

class Privileges:

    @staticmethod
    def default():
        return Privileges("Allowed to add message", "Allowed to delete users", "Allowed to ban users")

    def __init__(self, *privileges) -> None:
        self.privileges = set(privileges)

    def add(self, priv):
        self.privileges |= priv

    def remove(self, priv):
        self.privileges -= priv

    def show_privileges(self):
        return ", ".join(self.privileges)


class Admin(User):

    def __init__(self, first_name, last_name, privileges = Privileges.default()) -> None:
        super().__init__(first_name, last_name)
        self.privileges = privileges
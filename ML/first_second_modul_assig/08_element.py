class Element:
    def __init__(self, name, symbol, number) -> None:
        self._name = name
        self._symbol = symbol
        self._number = number

    @property
    def name(self):
        return self._name

    @property
    def symbol(self):
        return self._symbol

    @property
    def number(self):
        return self._number


    def __repr__(self) -> str:
        return f"Element('{self.name}', '{self.symbol}', {self.number})"

    def __str__(self):
        return repr(self)

hydrogen = Element('Hydrogen', 'H', 1)


data = {
    'name': 'Hydrogen',
    'symbol': 'H',
    'number': 1
}

hydrogen = Element(**data)
#hydrogen.dump()
print(hydrogen)
print(hydrogen.name)


from sys import argv

CIPHER_KEY = 'aaaaabbbbbabbbaabbababbaaababaab'

ALPHABET = { CIPHER_KEY[i:i+5]: letter for i, letter in enumerate('abcdefghijklmnopqrstuvwxyz') }


def each_five(s):
    five = ''
    for letter in s:
        if letter.isspace():
            continue
        five += letter
        if len(five) == 5:
            yield  five
            five = ''

def a_b_code(string):
    for token in each_five(string):
#        print(token)
        code = ''
        for letter in token:
            code += 'b' if letter.isupper() else 'a'
        yield code


def beacons_decode(string: str):
    word =''
    for token in a_b_code(string):
        word += ALPHABET[token]
    return word


print(beacons_decode(argv[1]))

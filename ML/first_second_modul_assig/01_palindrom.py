# task_1
from sys import argv

def palindrom(args):
    string = "".join(args).lower().replace(' ','')
    s_len = len(string)
    for i in range(0, int(s_len/2)):
        if string[i] != string[s_len-i-1]:
            return "NO"
    return "YES"

print(palindrom(argv[1:]))
import pandas as pd
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

df = pd.read_csv('data.csv', skiprows=3)
df.columns = ['Date', 'Temperature']
print(df.Date.dtype)
df['Date'] = df['Date'] // 100

X = df.iloc[:, :-1]

print('X:')
print(X)

y = df.iloc[:, -1]
print('y:')
print(y)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)
lr = LinearRegression()
lr.fit(X_train, y_train)

y_lr_train_pred = lr.predict(X_train)
y_lr_test_pred = lr.predict(X_test)
print('y_lr_train_pred:')
print(y_lr_train_pred)
print('y_lr_test_pred:')
print(y_lr_test_pred)

lr_train_mse = mean_squared_error(y_train, y_lr_train_pred)
lr_train_r2 = r2_score(y_train, y_lr_train_pred)
lr_test_mse = mean_squared_error(y_test, y_lr_test_pred)
lr_test_r2 = r2_score(y_test, y_lr_test_pred)

lr_result = pd.DataFrame({'Method': ['Linear regression'],
                           'Training MSE': [lr_train_mse],
                           'Training R2': [lr_train_r2],
                           'Test MSE': [lr_test_mse],
                           'Test R2': [lr_test_r2]})
print(lr_result.to_markdown())


import numpy as np


plt.scatter(y_train, y_lr_train_pred, label='Обучающий набор', alpha=0.7)

print('\ny_train, y_lr_train_pred:')
df  = pd.DataFrame(np.array([y_train, y_lr_train_pred, ((y_train - y_lr_train_pred)**2)**.5]).T)
df.columns = ['train', 'pred', 's']
df = df.sort_values('train')
print(df.to_markdown())

# Создаем график для тестового набора данных
plt.scatter(y_test, y_lr_test_pred, label='Тестовый набор', alpha=0.7)

# Генерируем значения для линии идентичности
identity_line = np.linspace(min(min(y_train), min(y_test)), max(max(y_train), max(y_test)), 100)

# Добавляем линию идентичности (y = x)
plt.plot(identity_line, identity_line, color='red', linestyle='--', linewidth=2, label='Идентичность')

plt.xlabel('Фактические значения')
plt.ylabel('Прогнозные значения')
plt.legend()
plt.title('График фактических значений vs. прогнозных значений')

plt.show()







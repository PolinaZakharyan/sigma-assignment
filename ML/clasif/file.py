
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats

#import seaborn


c = lambda f: 5/9 * (f - 32)
temps = [(f, c(f)) for f in range(0, 101, 10)]

temps_df = pd.DataFrame(temps, columns=['Fahrenheit', 'Celsius'])
axes = temps_df.plot(x='Fahrenheit', y='Celsius', style='.-')
y_label = axes.set_ylabel('Celsius')

nyc = pd.read_csv('ave_hi_nyc_jan_1895-2018.csv')
nyc.columns = ['Date', 'Temperature']
nyc = nyc.iloc[3:]
nyc['Temperature'] = nyc['Temperature'].astype(float)
print(nyc.Date.dtype)

nyc['Date'] = nyc['Date'].str[:-2]
pd.options.display.float_format = '{:.2f}'.format
nyc['Date'] = nyc['Date'].astype(float)

linear_regression = stats.linregress(x=nyc.Date, y=nyc.Temperature)



print(nyc.head())
print(nyc.tail())
print(nyc.Temperature.describe())
print(linear_regression.slope)
print(linear_regression.intercept)
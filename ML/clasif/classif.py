import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

x_label0 = np.random.normal(5, 1, 10)
x_label1 = np.random.normal(2, 1, 10)
xs = np.append(x_label0, x_label1).astype(np.float32)  # Привести к типу tf.float32
labels = [0.] * len(x_label0) + [1.] * len(x_label1)
plt.scatter(xs, labels)

learning_rate = 0.001
training_epochs = 1000

def model(X, w):
    return tf.add(tf.multiply(w[1], tf.pow(X, 1)),
                   tf.multiply(w[0], tf.pow(X, 0)))

w = tf.Variable([0., 0.], name="parameters", dtype=tf.float32)

for epoch in range(training_epochs):
    with tf.GradientTape() as tape:
        y_model = model(xs, w)
        current_cost = tf.reduce_sum(tf.square(labels - y_model))
    grads = tape.gradient(current_cost, [w])
    w.assign_sub(learning_rate * grads[0])
    
    if epoch % 100 == 0:
        print(epoch, current_cost.numpy())

w_val = w.numpy()
print('learned parameters', w_val)

x_label0 = np.append(np.random.normal(5, 1, 9), 20).astype(np.float32)  # Привести к типу tf.float32
correct_prediction = tf.equal(labels, tf.cast(tf.greater(y_model, 0.5), tf.float32))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
print('accuracy', accuracy.numpy())

all_xs = np.linspace(0, 10, 100).astype(np.float32)  # Привести к типу tf.float32
plt.plot(all_xs, all_xs * w_val[1] + w_val[0])
plt.show()

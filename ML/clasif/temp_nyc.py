import pandas as pd
from scipy import stats
import seaborn as sns
import matplotlib.pyplot as plt
from tabulate import tabulate


nyc = pd.read_csv('data.csv', skiprows=3)
nyc.columns = ['Date', 'Temperature']
print(nyc.head())

print(nyc.Date.dtype)
nyc['Date'] = nyc['Date'] // 100
print(nyc.tail())

nyc_2018 = nyc[nyc['Date'] <= 2018]
print(nyc_2018.head())

sns.set_style('whitegrid')
axes = sns.regplot(x=nyc_2018.Date, y=nyc_2018.Temperature)
axes.set_ylim(10, 70)

plt.title('New York Average Temperature')
plt.xlabel('Date')
plt.ylabel('Temperature')

# Отобразите график
plt.show()

linear_regression = stats.linregress(x=nyc_2018.Date, y=nyc_2018.Temperature)

print("\n")
def forecast(x):
    return linear_regression.slope * x + linear_regression.intercept



years = [2019, 2020, 2021, 2022, 1890, 1891, 1892, 1893, 1894]
years = { y: forecast(y) for y in years }

print(tabulate(years.items()))



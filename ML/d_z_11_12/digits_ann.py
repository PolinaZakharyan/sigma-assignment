from keras.datasets import mnist
import cv2
from keras.utils import to_categorical
from keras import models
from keras import layers
import matplotlib.pyplot as plt



(train_images, train_labels), (test_images, test_labels) = mnist.load_data()


plt.imshow(train_images[1], cmap='gray')
plt.title(f'Label: {train_labels[1]}')
plt.show()


train_images = train_images.reshape((60000, 28 * 28))
train_images = train_images.astype('float32') / 255
test_images = test_images.reshape((10000, 28 * 28))
test_images = test_images.astype('float32') / 255

train_labels = to_categorical(train_labels)
test_labels = to_categorical(test_labels)

network = models.Sequential()
network.add(layers.Dense(512, activation='relu', input_shape=(28 * 28,)))
network.add(layers.Dense(10, activation='softmax'))

network.compile(optimizer='RMSProp', loss='categorical_crossentropy', metrics=['accuracy'])

network.fit(train_images, train_labels, epochs=5, batch_size=128)

test_loss, test_acc = network.evaluate(test_images, test_labels)

network.save('my_model.h5')
json_string = network.to_json()
network.save_weights('my_model_weights.h5')
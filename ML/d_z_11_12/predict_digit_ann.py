from keras.models import load_model
import cv2
model = load_model('my_model.h5')

tst = 255 - cv2.imread('digit_3.png', 0)
tst = cv2.resize(tst,(28, 28))

tst = tst.reshape((1, 28 * 28))
tst = tst.astype('float32') / 255

pred=list(model.predict(tst)[0])
print(pred.index(max(pred)))

"""
Conclusion:
During the project, the model demonstrated good accuracy on test data,
but additional improvements may be needed to enhance accuracy in recognizing
self-hand-drawn numbers. Possible directions for further research include improving
data preprocessing and fine-tuning model parameters.

"""
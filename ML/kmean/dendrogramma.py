import numpy as np
from scipy.cluster.hierarchy import dendrogram, linkage
import matplotlib.pyplot as plt

#           Боевики | Детективы | Комедии | Мелодрамы | Пуаро | Коломбо
#Любитель 1:     1    |     0     |    1    |     0     |   0   |    1
#Любитель 2:     1    |     0     |    0    |     1     |   0   |    0
#Любитель 3:     0    |     1     |    1    |     0     |   1   |    0
#...


# Создаем матрицу данных (замените эту матрицу вашими данными)
data_matrix = np.random.randn(200, 6)
    # Дополните матрицу данными о других любителях фильмов


# Строим дендрограмму с помощью linkage и dendrogram
linkage_matrix = linkage(data_matrix, method='ward')  # Метод 'ward' для объединения кластеров
#dendrogram(linkage_matrix, labels=range(len(data_matrix)))

# Строим дендрограмму с помощью linkage
#linkage_matrix = linkage(data_matrix, method='ward')  # Метод 'ward' для объединения кластеров

# Отрисовываем дендрограмму с подписями кластеров
plt.figure(figsize=(10, 6))
dendrogram(linkage_matrix, labels=range(len(data_matrix)), leaf_font_size=10)


# Настраиваем внешний вид дендрограммы
plt.xlabel('Любители фильмов')
plt.ylabel('Расстояние')
plt.title('Дендрограмма интересов к фильмам')
plt.show()

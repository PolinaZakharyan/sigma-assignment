from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score

# Загрузка данных Iris
iris = load_iris()
X = iris.data  # Матрица признаков
y = iris.target  # Вектор меток классов

# Разделение данных на тренировочный и тестовый наборы
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Инициализация Gaussian Naive Bayes классификатора
clf = GaussianNB()

# Обучение классификатора на тренировочных данных
model = clf.fit(X_train, y_train)

# Предсказание классов на тестовом наборе данных
y_pred = clf.predict(X_test)
print(y_pred)
print(y_test)
# Оценка точности модели
accuracy = accuracy_score(y_test, y_pred)
print("Точность модели на тестовых данных: {:.2f}%".format(accuracy * 100))

new_observation = [[4, 4, 4, 0.4]]
print(model.predict(new_observation))

classifer = GaussianNB(priors=[0.1,0.4,0.5])
model_1= classifer.fit(X_train, y_train)
new_observation = [[4, 4, 4, 0.4]]
print(model_1.predict(new_observation))


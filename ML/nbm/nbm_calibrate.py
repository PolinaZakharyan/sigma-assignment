# Загрузка библиотек
from sklearn import datasets
from sklearn.naive_bayes import GaussianNB
from sklearn.calibration import CalibratedClassifierCV

# Загрузка данных Iris
iris = datasets.load_iris()
features = iris.data
target = iris.target

# Создание объекта Gaussian Naive Bayes
classifier = GaussianNB()

# Создание калиброванного кросс-валидационного классификатора с сигмоидной калибровкой
classifier_sigmoid = CalibratedClassifierCV(classifier, cv=2, method='sigmoid')

# Калибровка вероятностей
classifier_sigmoid.fit(features, target)

# Создание нового наблюдения
new_observation = [[2.6, 2.6, 2.6, 0.41]]

# Просмотр калиброванных вероятностей
probabilities = classifier_sigmoid.predict_proba(new_observation)
print(probabilities)

# Train a Gaussian naive Bayes then predict class probabilities 
classifier.fit(features, target).predict_proba(new_observation) 
#array([[ 2. 58229098e- 04 9. 99741447e- 01, 3 23523643-0]]) 

#View calibrated probabilities 
classifier_sigmoid.predict_proba(new_observation)
#array([[ 31859969, 63663466, 04476565]])

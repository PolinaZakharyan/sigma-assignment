#Load librartes
import numpy as np
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer

#Create text
text_data = np.array(['I love Brazil. Brazil!',
                      'Brazil is best', 
                      'Germany beats both'])

#Create bag of words
count = CountVectorizer()
bag_of_words = count.fit_transform(text_data)

#Create feature matrix
features = bag_of_words.toarray()

#Create target vector
target = np.array([0,0,1])

#Create multinomtal natve Bayes object wtth prtor probabilities of each class
classifer = MultinomialNB(class_prior=[0.25, 0.5])

#Train model
model = classifer.fit(features, target)

new_observation = [[0,0,0,1,0,1,0]]

print(model.predict(new_observation))
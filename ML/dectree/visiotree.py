#Load libraries
import pydotplus
from sklearn.tree import DecisionTreeClassifier
from sklearn import datasets
import os
from sklearn import tree

#Load data
iris = datasets.load_iris()
features = iris.data
target = iris.target

#Create decision tree classifier object
decisiontree = DecisionTreeClassifier(random_state=0)

#Train model
model = decisiontree.fit(features, target)
# Create DOT data
dot_data = tree.export_graphviz(decisiontree, out_file=None, feature_names=iris.feature_names,
                                class_names=iris.target_names)
#Draw graph
graph = pydotplus.graph_from_dot_data(dot_data)


# Create a PNG file containing the decision tree graph
graph.write_png("decision_tree.png")

# Display the saved PNG file
os.system("xdg-open decision_tree.png")
#import numpy as np
#
#
#X = np.array([[0, 0],
#              [0, 1],
#              [1, 0],
#              [1, 1]])
#
#y = np.array([0, 1, 1, 0])
#
#import matplotlib.pyplot as plt
#
#plt.scatter(X[y == 1, 0], X[y == 1, 1], c='b', marker='x', label='Class 1')
#plt.scatter(X[y == 0, 0], X[y == 0, 1], c='r', marker='s', label='Class 0')
#plt.xlabel("Feature 1")
##plt.ylabel("Feature 2")
##plt.legend(loc='best')
##plt.title("XOR Data Visualization")
##plt.show()
#
#import numpy as np
#
## Коэффициенты гиперплоскости
#a = 1
#b = -2
#c = -1
#
## Координаты точки P
#x_p = 1
#y_p = 2
#
## Вычисляем скалярное произведение между нормальным вектором и вектором P
#result = a * x_p + b * y_p + c
#
## Проверяем положение точки P относительно гиперплоскости
#if result > 0:
#    print("Точка P находится с одной стороны от гиперплоскости.")
#elif result < 0:
#    print("Точка P находится с другой стороны от гиперплоскости.")
#else:
#    print("Точка P лежит на гиперплоскости.")
import numpy as np
from sklearn.svm import LinearSVC
from sklearn.preprocessing import StandardScaler

# Обучающие данные
X = np.array([[2, 3], [1, 1], [4, 5], [5, 4]])
y = np.array([1, -1, 1, -1])

# Масштабирование признаков
scaler = StandardScaler()
X_scaled = scaler.fit_transform(X)

# Создание и обучение модели LinearSVC
C = 0.5
model = LinearSVC(C=C, random_state=0)
model.fit(X_scaled, y)

# Потери для каждого обучающего примера
losses = []
for i in range(len(X)):
    xi = X_scaled[i]
    yi = y[i]
    fi = model.decision_function([xi])[0]
    hinge_loss = max(0, 1 - yi * fi)
    losses.append(hinge_loss)

print("Потери для каждого обучающего примера:", losses)





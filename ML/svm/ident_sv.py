#Load libraries
from sklearn.svm import SVC
from sklearn import datasets
from sklearn.preprocessing import StandardScaler
import numpy as np

#Load data with only two classes
iris = datasets.load_iris()
features = iris.data[:100,:]
target = iris.target[:100]

#Standardize features
scaler = StandardScaler()
features_standardized = scaler.fit_transform(features)

#Create support vector classifier object
svc = SVC(kernel="linear", random_state=0)

#Train classifter
model = svc.fit(features_standardized, target)

#View support vectors
print(model.support_vectors_)

print(model.support_)
print(model.n_support_)
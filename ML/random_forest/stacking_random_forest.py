import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

# Создаем простой DataFrame с данными о домах, включая цену и категориальные признаки.
data = pd.DataFrame({
    'Количество спален': [3, 2, 4, 3, 3],
    'Количество ванных комнат': [2, 1, 3, 2, 2],
    'Площадь (кв. футы)': [1800, 1200, 2200, 1600, 1700],
    'Гараж': ['Да', 'Нет', 'Да', 'Да', 'Нет'],
    'Бассейн': ['Да', 'Нет', 'Да', 'Нет', 'Да'],
    'Цена ($)': [300000, 180000, 360000, 250000, 280000]
})

# Преобразуем категориальные признаки в числовые с помощью One-Hot Encoding
data = pd.get_dummies(data, columns=['Гараж', 'Бассейн'])

# Разделяем данные на признаки (X) и целевую переменную (y)
X = data.drop('Цена ($)', axis=1)
y = data['Цена ($)']

# Разделяем данные на обучающий и тестовый наборы
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Создаем две базовые модели: случайный лес и линейную регрессию
random_forest = RandomForestRegressor(random_state=42)
linear_regression = LinearRegression()

# Обучаем базовые модели на обучающем наборе данных
random_forest.fit(X_train, y_train)
linear_regression.fit(X_train, y_train)

# Генерируем прогнозы базовых моделей на тестовом наборе данных
rf_predictions = random_forest.predict(X_test)
lr_predictions = linear_regression.predict(X_test)

# Создаем новую матрицу признаков для мета-модели из прогнозов базовых моделей
stacked_features = pd.DataFrame({
    'Случайный лес': rf_predictions,
    'Линейная регрессия': lr_predictions
})

# Обучаем мета-модель (например, линейная регрессия) на матрице признаков мета-модели
meta_model = LinearRegression()
meta_model.fit(stacked_features, y_test)

# Генерируем прогнозы мета-модели
meta_predictions = meta_model.predict(stacked_features)

# Оцениваем качество стекинга
mse = mean_squared_error(y_test, meta_predictions)
print("Среднеквадратичная ошибка стекинга:", mse)

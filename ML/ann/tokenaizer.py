from keras.preprocessing.text import Tokenizer

# Создаем объект токенизатора
tokenizer = Tokenizer()

# Тексты для токенизации
texts = [
    "Кофе вкусен и ароматен.",
    "Чай тоже хорош, но не такой ароматный.",
    "Я люблю вино, оно такое классное."
]

# Фитим (обучаем) токенизатор на текстах
tokenizer.fit_on_texts(texts)

# Получаем словарь токенов и их индексов
word_index = tokenizer.word_index
word_index = {word: index + 1 for word, index in word_index.items()}

print("Словарь токенов и их индексы:")
print(word_index)

# Преобразуем тексты в последовательности индексов
sequences = tokenizer.texts_to_sequences(texts)
print("\nТексты в виде последовательностей индексов:")
print(sequences)

# Преобразуем тексты в последовательности индексов
sequences = tokenizer.texts_to_sequences(texts)

# Преобразуем последовательности в матрицу бинарных признаков
matrix = tokenizer.sequences_to_matrix(sequences, mode='binary')

print(matrix)
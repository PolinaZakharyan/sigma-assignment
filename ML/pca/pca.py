import numpy as np
import matplotlib.pyplot as plt

data = np.array([
    [80, 2, 0.2, 1.5],
    [10, 1.5, 0.3, 0.5],
    [30, 3, 0.5, 2],
    [0, 0, 100, 0],
    [0, 0, 25, 20]
])

# Вычисление средних значений для каждого признака
mean = np.mean(data, axis=0)

# Вычисление стандартных отклонений для каждого признака
std_dev = np.std(data, axis=0)

# Стандартизация данных
standardized_data = (data - mean) / std_dev

cov_matrix = np.cov(standardized_data, rowvar=False)

eigenvalues, eigenvectors = np.linalg.eig(cov_matrix)


# Сортировка собственных значений в убывающем порядке
sorted_indices = np.argsort(eigenvalues)[::-1]
sorted_eigenvalues = eigenvalues[sorted_indices]
sorted_eigenvectors = eigenvectors[:, sorted_indices]

# Выбор двух главных компонент
num_components = 2
num_components = 2

#pc1, pc2 = sorted_eigenvectors[:, :num_components][:2]
#pc1, pc2
# Проекция данных на главные компоненты
projected_data = standardized_data.dot(sorted_eigenvectors[:, :num_components])
pc1 = projected_data[:, 0]  # Первая главная компонента
pc2 = projected_data[:, 1]
#projected_data = standardized_data.dot(pc1, pc2)


# Предположим, у вас есть данные после анализа PCA, и вы хотите визуализировать их.
# Замените 'pc1' и 'pc2' на ваши главные компоненты или признаки, которые вы хотите визуализировать.

# Создайте фигуру и оси для графика
plt.figure(figsize=(8, 6))

# Визуализация данных в двумерном пространстве
plt.scatter(pc1, pc2, c='blue', marker='o', label='Продукты')

# Добавление подписей к точкам (продуктам)
products = ['Апельсин', 'Яблоко', 'Брокколи', 'Масло', 'Свинина']
for i, product in enumerate(products):
    plt.annotate(product, (pc1[i], pc2[i]), textcoords="offset points", xytext=(0,10), ha='center')

# Добавление подписей к осям
plt.xlabel('Главная компонента 1')
plt.ylabel('Главная компонента 2')

# Добавление заголовка и легенды
plt.title('Визуализация данных продуктов после PCA')
plt.legend(loc='best')

# Отображение графика
plt.show()
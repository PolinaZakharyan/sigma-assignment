from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from PIL import Image
import numpy as np
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt


digits = load_digits()
print(digits.DESCR)
print(digits.target[::100])

print(digits.data.shape)

print(digits.target.shape)

print(digits.images[13])

print(digits.data[13])

figure, axes =plt.subplots(nrows=4,ncols=6,figsize=(6, 4))
#figure, axes =plt.subplots(nrows=6,ncols=6,figsize=(6, 4))
for item in zip(axes.ravel(), digits.images, digits.target):

    axes, image, target = item
    axes.imshow(image, cmap=plt.cm.gray_r)

    axes.set_xticks([])
    axes.set_yticks([])

    axes.set_title(target)
    plt.tight_layout()
    #plt.show()

X_train, X_test, y_train, y_test = train_test_split(digits.data, digits.target, random_state=11, test_size=0.20)
X_train.shape
X_test.shape

knn = KNeighborsClassifier()
knn.fit(X=X_train, y=y_train)

predicted = knn.predict(X=X_test)
expected = y_test
print(predicted[:36])
print(expected[:36])
print(f'{knn.score(X_test, y_test):.2%}')

confusion = confusion_matrix(y_true=expected, y_pred=predicted)
print(confusion)

names = [str(digit) for digit in digits.target_names]
print(classification_report(expected, predicted, target_names=names))


pipe = Pipeline([("knn", knn)])
search_space = [{"knn__n_neighbors": [1,2,3,4,5,6,7,8,9,10]}]

classifier = GridSearchCV(pipe, search_space, cv=5, verbose=0).fit(X=X_train, y=y_train)
best_k = classifier.best_estimator_.get_params()["knn__n_neighbors"]
print(best_k)

knn_1 = KNeighborsClassifier(n_neighbors=4, n_jobs=-1)
knn_1.fit(X=X_train, y=y_train)
print(f'{knn_1.score(X_test, y_test):.2%}')

knn_2 = KNeighborsClassifier(n_neighbors=4, weights='distance', metric='minkowski', n_jobs=-1)
knn_2.fit(X=X_train, y=y_train)

predicted = knn_2.predict(X=X_test)
expected = y_test
print(predicted[:36])
print(expected[:36])

svc = SVC(kernel="rbf")
model = svc.fit(X_train, y_train)

predicted = svc.predict(X=X_test)
expected = y_test
print(predicted[:36])
print(expected[:36])

print(f'{svc.score(X_test, y_test):.2%}')

confusion = confusion_matrix(y_true=expected, y_pred=predicted)
print(confusion)

names = [str(digit) for digit in digits.target_names]
print(classification_report(expected, predicted, target_names=names))

image = Image.open('digit_6.png')  # Замените на путь к вашему изображению
image = image.convert('L')  # Преобразуйте в оттенки серого
image = image.resize((8, 8))  # Измените размер на 8x8 пикселей (если модель ожидает такой размер)
# print(image)

image_array = np.array((image) , dtype=np.float32)
image_array *= (16/256)
np.round(image_array, out=image_array)
image_array = 16 - image_array
print(image_array)
image_array = image_array.flatten()

predicted_digit = knn.predict([image_array])
print(f'Predicted Digit: {predicted_digit[0]}')

predicted_digit = svc.predict([image_array])
print(f'Predicted Digit: {predicted_digit[0]}')


